<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Articulos</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


  </head>
  <body>
    <?php include 'navbar.php' ?>

    <!-- Contenido -->
    <div class="container">

      <div class="card bg-light mb-12" style="margin-top:3em;">
        <div class="card-header" style="background-color:#007bff;color:#fff;">
          <div class="col-md-12" align="right">
            <button type="button" class="btn btn-success" id="btnNuevoArticulo" name="btnNuevoArticulo" align="right" data-toggle="modal" data-target="#modNuevoCliente">
              <i class="fa fa-plus"></i> Nuevo Articulo</button>
          </div>
        </div>
        <div class="card-body">

          <h5 class="card-title">Articulos Registrados</h5>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Clave Articulo</th>
                <th scope="col">Descripcion</th>
                <th scope="col" style="text-align:center;">Editar</th>
              </tr>
            </thead>
            <tbody id="tbodyArticulos">


            </tbody>
        </table>

        </div>
      </div>


      <div class="modal" id="modNuevoCliente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Registro de Clientes</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="">
                <form class="form" id="formNuevoArticulo">
                  <input type="hidden" name="id" id="identificador" value="0">
                  <input type="hidden" name="accion" value="2">
                  <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label">Descripción</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <input type="text" class="form-control" id="descripcion" name="descripcion">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="apPaterno" class="col-sm-2 col-form-label">Modelo</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <input type="text" class="form-control" id="modelo" name="modelo">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="apMaterno" class="col-sm-2 col-form-label">Precio</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <input type="number" class="form-control" id="precio" name="precio" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="rfc" class="col-sm-2 col-form-label">Existencia</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <input type="number" class="form-control" id="existencia" name="existencia" >
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" id="CanelarCli" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" id="GuardarCli" class="btn btn-primary" >Guardar</button>
            </div>
          </div>
        </div>
      </div>




    </div>
    <!-- Contenido -->
    <script type="text/javascript">

    llenarTabla();
    function llenarTabla() {
      $("#tbodyArticulos").empty();
      var datos = {
        "accion":1
      }
      $.post("servidor/articulos.php",datos,function(data){
        if (data.error) {
          alert("No hay articulos registrados")
        }else {
          $.each(data.datos,function (i,item) {
            $("#tbodyArticulos").append("<tr>"+
                                          "<td>"+item.id+"</td>"+
                                          "<td>"+item.descripcion+"</td>"+
                                          '<td style="text-align:center;">'+
                                          '<button class="btn btn-primary editarArticulo" id="'+item.id+'" descripcion="'+item.descripcion+'" '+
                                                                              'precio="'+item.precio+'" existencia="'+item.existencia+'" modelo="'+item.modelo+'"'+
                                          ' ><span class="fa fa-edit"></span></button>'+
                                          '</td>'+
                                      "<tr>")
          })

        }
      },"json").fail(function(e,ex,error) {
        alert(error)
      })
    }


          $("#GuardarCli").on("click",function(e){
            $("#formNuevoArticulo").submit();
          })

          $("#formNuevoArticulo").on("submit",function(e){
            e.preventDefault();
            let datos = $(this).serialize();
            $.post("servidor/articulos.php",datos,function(data){
              if (data.error) {
                alert(data.msg)
              }else {
                alert(data.msg)
                $('#formNuevoArticulo').trigger("reset");
                $("#modNuevoCliente").modal('hide');
                llenarTabla();
              }
            },"json").fail(function(e,ex,error) {
              alert(error)
            })
          })

          $(document).on("click",".editarArticulo",function (e) {
            let boton = $(this);
            $("#modNuevoCliente").modal("show");
            $("#identificador").val(boton.attr("id"))
            $("#descripcion").val(boton.attr("descripcion"))
            $("#modelo").val(boton.attr("modelo"))
            $("#precio").val(boton.attr("precio"))
            $("#existencia").val(boton.attr("existencia"))
          })

          $('#modNuevoCliente').on('hidden.bs.modal', function (e) {
            $('#formNuevoArticulo').trigger("reset");
            $("#identificador").val(0);
          })
    </script>
  </body>
</html>
