<nav class="navbar navbar-expand-lg navbar-dark bg-dark">


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" style="color:#fff;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Inicio
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="ventas.php">Ventas</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="clientes.php">Clientes</a>
          <a class="dropdown-item" href="articulos.php">Articulos</a>
          <a class="dropdown-item" href="configuracion.php">Configuración</a>
        </div>
      </li>


    </ul>
  </div>

  <div class="navbar-collapse collapse justify-content-between">
     <ul class="navbar-nav mr-auto">
     </ul>
     <ul class="navbar-nav">
       <font style="color:#fff;">Fecha: <?php echo Date('d-m-Y') ?></font>
     </ul>
 </div>
</nav>
