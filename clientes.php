<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Configuración</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


  </head>
  <body>
    <?php include 'navbar.php' ?>

    <!-- Contenido -->
    <div class="container">

      <div class="card bg-light mb-12" style="margin-top:3em;">
        <div class="card-header" style="background-color:#007bff;color:#fff;">
          <div class="col-md-12" align="right">
            <button type="button" class="btn btn-success" id="btnNuevoCliente" name="btnNuevoCliente" align="right" data-toggle="modal" data-target="#modNuevoCliente">
              <i class="fa fa-plus"></i> Nuevo Cliente</button>
          </div>
        </div>
        <div class="card-body">

          <h5 class="card-title">Clientes Registrados</h5>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Clave Cliente</th>
                <th scope="col">Nombre</th>
                <th scope="col">Editar</th>
              </tr>
            </thead>
            <tbody id="tbodyClientes">


            </tbody>
        </table>

          <!--
          <form id="frmConfiguracion" action="">
            <input type="hidden" name="accion" value="2">
            <div class="form-group row">
              <label for="tasa" class="col-sm-2 col-form-label">Nombre</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="tasa" name="tasa">
              </div>
            </div>
            <div class="form-group row">
              <label for="enganche" class="col-sm-2 col-form-label">Apellido Paterno</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="enganche" name="enganche">
              </div>
            </div>

            <div class="form-group row">
              <label for="plazo" class="col-sm-2 col-form-label">Apellido Materno</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="plazo" name="plazo" >
              </div>
            </div>

            <div class="form-group row">
              <label for="plazo" class="col-sm-2 col-form-label">RFC</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="plazo" name="plazo" >
              </div>
            </div>

            <div align="right">
              <button type="button" onclick="location.href='index.php';" class="btn btn-danger">Cancelar</button>
              <button type="submit" class="btn btn-success">Guardar</button>

            </div>

          </form>
        -->
        </div>
      </div>


      <div class="modal" id="modNuevoCliente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Registro de Clientes</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="form" id="formNuevoCliente">
                <input type="hidden" name="accion" value="2">
                <div class="form-group row">
                  <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="nombre" name="nombre">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="apPaterno" class="col-sm-2 col-form-label">Apellido Paterno</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="apPaterno" name="apPaterno">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="apMaterno" class="col-sm-2 col-form-label">Apellido Materno</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="apMaterno" name="apMaterno" >
                  </div>
                </div>

                <div class="form-group row">
                  <label for="rfc" class="col-sm-2 col-form-label">RFC</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="rfc" name="rfc" >
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="CanelarCli" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" id="GuardarCli" class="btn btn-primary" >Guardar</button>
            </div>
          </div>
        </div>
      </div>




    </div>
    <!-- Contenido -->
    <script type="text/javascript">

    llenarTabla();
    function llenarTabla() {
      $("#tbodyClientes").empty();
      var datos = {
        "accion":1
      }
      $.post("servidor/clientes.php",datos,function(data){
        if (data.error) {
          alert("No hay clientes registrados")
        }else {
          $.each(data.datos,function (i,item) {
            $("#tbodyClientes").append("<tr>"+
                                          "<td>"+item.id+"</td>"+
                                          "<td>"+item.nombre+" "+item.apPaterno+" "+item.apMaterno+"</td>"+
                                          '<td>'+
                                          '<button class="btn btn-primary"><span class="fa fa-edit"></span></button>'+
                                          '</td>'+
                                      "<tr>")
          })

        }
      },"json").fail(function(e,ex,error) {
        alert(error)
      })
    }


          $("#GuardarCli").on("click",function(e){
            $("#formNuevoCliente").submit();
          })

          $("#formNuevoCliente").on("submit",function(e){
            e.preventDefault();
            let datos = $(this).serialize();
            $.post("servidor/clientes.php",datos,function(data){
              if (data.error) {
                alert(data.msg)
              }else {
                alert(data.msg)
                $('#formNuevoCliente').trigger("reset");
                $("#modNuevoCliente").modal('hide');
                llenarTabla();
              }
            },"json").fail(function(e,ex,error) {
              alert(error)
            })
          })

    </script>
  </body>
</html>
