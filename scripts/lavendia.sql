
use lavendimia;

drop table if exists ventas;
create table ventas(
	id int primary key auto_increment,
    cliente int, 
    total double(10,2),
    fecha datetime,
    estado int
);

drop table if exists clientes;
create table clientes(
	id int primary key auto_increment,
    nombre nvarchar(250), 
    apPaterno nvarchar(250), 
    apMaterno nvarchar(250), 
    rfc nvarchar(50)
);

drop table if exists articulos;
create table articulos(
	id int primary key auto_increment,
    descripcion nvarchar(250), 
    modelo nvarchar(250), 
    precio double(10,2), 
    existencia int
);

drop table if exists configuracion;
create table configuracion(
	id int primary key auto_increment,
    tasa double(10,2), 
    enganche int,  
    plazo int
);


drop table if exists configuracion;
create table configuracion(
	id int primary key auto_increment,
    tasa double(10,2), 
    enganche int,  
    plazo int
);

