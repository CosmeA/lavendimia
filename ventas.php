<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ventas</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


  </head>
  <body>
    <?php include 'navbar.php' ?>

    <!-- Contenido -->
    <div class="container">

      <div class="card bg-light mb-12" style="margin-top:3em;">
        <div class="card-header" style="background-color:#007bff;color:#fff;">
          <div class="col-md-12" align="right">
            <button type="button" class="btn btn-success" id="btnNuevaVenta" name="btnNuevaVenta" align="right" data-toggle="modal" data-target="#modNuevaVenta">
              <i class="fa fa-plus"></i> Nueva Venta</button>
          </div>
        </div>
        <div class="card-body">
          <h5 class="card-title">Ventas Activas</h5>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Folio Venta</th>
                <th scope="col">Clave Cliente</th>
                <th scope="col">Nombre</th>
                <th scope="col">Total</th>
                <th scope="col">Fecha</th>
                <th scope="col">Estatus</th>
              </tr>
            </thead>
            <tbody id=tbodyVentas>


            </tbody>
        </table>
        </div>
      </div>

      <div class="modal" id="modNuevaVenta" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Registro de Ventas</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            <form class="form" id="formNuevaVenta">
                <input type="hidden" name="accion" value="2">
                <div class="form-group row">
                  <label for="cliente" class="col-sm-2 col-form-label">Cliente</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <select class="form-control" name="cliente" id="cliente">

                    </select>

                  </div>
                </div>
                <div class="form-group row">
                  <label for="articulo" class="col-sm-2 col-form-label">Articulo</label>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <select class="form-control" name="articulo" id="articulo">
			<option disabled selected>Seleccione un producto</option>	
                    </select>
                  </div>
                </div>


            <br>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Descripci&oacute;n Articulo</th>
                  <th scope="col">Modelo</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Importe</th>
                </tr>
              </thead>
              <tbody id=tbodyLista>




              </tbody>
              <tfoot>
                <tr>
                  <td colspan="3" style="text-align:right">Total</td>
                  <td ></td>
                  <input type="hidden" id="importeinput" name="importe" value="">
                  <td id="totalimporte"></td>
                </tr>
              </tfoot>
          </table>
        </form>

            </div>
            <div class="modal-footer">
              <button type="button" id="CanelarVta" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" id="GuardarVta" class="btn btn-primary" >Guardar</button>
            </div>
          </div>
        </div>
      </div>


    </div>
    <!-- Contenido -->
    <script type="text/javascript">
      
	function llenarTabla(){
      var datos = {
        "accion":1
      }
      $.post("servidor/ventas.php",datos,function(data){
        $.each(data.clientes,function(i,item){
          $("#cliente").append(new Option(item.nombre+" "+item.apPaterno+" "+item.apMaterno, item.id));
        })

        $.each(data.articulos,function(i,item){
          $("#articulo").append(new Option(item.descripcion, item.id));
          $("#articulo option:last-child").attr("precio",item.precio);
          $("#articulo option:last-child").attr("existencia",item.existencia);
          $("#articulo option:last-child").attr("modelo",item.modelo);

        })

        if (data.error) {
          alert("No hay ventas disponibles")
        }else {
          $.each(data.datos,function (i,item) {
            $("#tbodyVentas").append("<tr>"+
                                        "<td>"+item.id+"</td>"+
                                        "<td>"+item.cliente+"</td>"+
                                        "<td>"+item.nombre+"</td>"+
                                        "<td>"+item.total+"</td>"+
                                        "<td>"+item.fecha+"</td>"+
                                        "<td>"+item.estado+"</td>"+
                                      "<tr>")
          })

        }
      },"json").fail(function(e,ex,error) {
        alert(error)
      })
}
	llenarTabla();	

      $("#GuardarVta").on("click",function(e){
        $("#formNuevaVenta").submit();
      })

      $("#formNuevaVenta").on("submit",function(e){
        e.preventDefault();
        let datos = $(this).serialize();
        $.post("servidor/ventas.php",datos,function(data){
          console.log(data);
	      if (data.error) {
                alert(data.msg)
              }else {
                alert(data.msg)
                $('#formNuevoCliente').trigger("reset");
                $("#modNuevaVenta").modal('hide');
                llenarTabla();
              }
       
	},"json").fail(function(e,ex,error) {
          alert(error)
        })
      });

      let count = 0;
      $("#articulo").on("change",function(e){
        let select = $("#articulo option:selected");
        $("#tbodyLista").append('<tr>'+
                                  '<td><input type="hidden" name="articulo['+count+'][producto]" value="'+select.val()+'">'+select.text()+'</td>'+
                                  '<td>'+select.attr("modelo")+'</td>'+
                                  '<td style="width:20px;"><input type="number" min="0" max='+select.attr("existencia")+' name="articulo['+count+'][cantidad]"  class="form-control cantidad" value="1"></td>'+
                                  '<td><input type="hidden" class="precio" name="articulo['+count+'][precio]" value="'+select.attr("precio")+'">'+select.attr("precio")+'</td>'+
                                  '<td><input type="hidden" class="importe" name="articulo['+count+'][importe]" value="'+select.attr("precio")+'"><font class="importeview">'+select.attr("precio")+'</font></td>'+
                                '</tr>');
                                sumarimporte();
                                count++;
      })

      $(document).on("keyup",".cantidad",function (e) {
        let cantidad = $(this).val();
        let row = $(this).closest("tr");
        let precio = row.find(".precio").val();
        row.find(".importe").val( ( precio * cantidad) );
        row.find(".importeview").text( ( precio * cantidad) );
        sumarimporte();
      })

      $('#modNuevaVenta').on('hidden.bs.modal', function (e) {
        $('#formNuevaVenta').trigger("reset");
        $("#tbodyLista").empty();
      })

      function sumarimporte() {
        let total = 0;
        $(".importe").each(function(i,item){
          total += parseFloat(item.value);
        })
        $("#totalimporte").text(total.toFixed(2))
        $("#importeinput").val(total)

      }
    </script>
  </body>
</html>
