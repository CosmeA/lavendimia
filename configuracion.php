<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Configuración</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


  </head>
  <body>
    <?php include 'navbar.php' ?>

    <!-- Contenido -->
    <div class="container">

      <div class="card bg-light mb-12" style="margin-top:3em;">
        <div class="card-header" style="background-color:#007bff;color:#fff;">
          Configuración
        </div>
        <div class="card-body">

          <form id="frmConfiguracion" action="">
            <input type="hidden" name="accion" value="2">
            <input type="hidden" name="id" id="identificador" value="">
            <div class="form-group row">
              <label for="tasa" class="col-sm-2 col-form-label">Tasa financiamento</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="tasa" name="tasa">
              </div>
            </div>
            <div class="form-group row">
              <label for="enganche" class="col-sm-2 col-form-label">% Enganche</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="enganche" name="enganche">
              </div>
            </div>

            <div class="form-group row">
              <label for="plazo" class="col-sm-2 col-form-label">Plazo Máximo</label>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <input type="text" class="form-control" id="plazo" name="plazo" >
              </div>
            </div>

            <div align="right">
              <button type="button" onclick="location.href='index.php';" class="btn btn-danger">Cancelar</button>
              <button type="submit" class="btn btn-success">Guardar</button>

            </div>

          </form>

        </div>
      </div>

    </div>
    <!-- Contenido -->
    <script type="text/javascript">

      var datos = {
        "accion":1
      }
      $.post("servidor/configuracion.php",datos,function(data){
        if (data.error) {
          alert("No hay configuracion disponible")
        }else {
          $("#tasa").val(data.datos.tasa);
          $("#enganche").val(data.datos.enganche);
          $("#plazo").val(data.datos.plazo);
          $("#identificador").val(data.datos.id);
        }
      },"json").fail(function(e,ex,error) {
        alert(error)
      })


    $("#frmConfiguracion").on("submit",function(e){
      e.preventDefault();
      let datos = $(this).serialize();
      $.post("servidor/configuracion.php",datos,function(data){
        if (data.error) {
          alert("Error al consultar configuracion.")
        }else {
          alert("Configuración modificada correctamente")
        }
      },"json").fail(function(e,ex,error) {
        alert(error)
      })
    })

    </script>
  </body>
</html>
