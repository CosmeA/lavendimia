<?php

require 'conexion.php';

  $accion = $_POST["accion"];

  switch ($accion) {
    case 1:
      obtenerClientes();
      break;
    case 2:
     registrarCliente();
    default:
      break;
  }

function obtenerClientes()
{
  $respuesta = array();
  $clientes = getClientes();
  if (count($clientes) > 0) {
    $respuesta['error'] = false;
    $respuesta['datos'] = $clientes;
  }else {
    $respuesta['error'] = true;
    $respuesta['datos'] = array();
  }
  echo json_encode($respuesta);
}



function registrarCliente()
{
    $respuesta = array();

    if (isset($_POST['nombre']) && !empty($_POST['nombre']) &&
        isset($_POST['apPaterno']) && !empty($_POST['apPaterno']) &&
        isset($_POST['apMaterno']) && !empty($_POST['apMaterno']) &&
        isset($_POST['rfc']) && !empty($_POST['rfc']) ) {
        $insertar = insertarCliente($_POST['nombre'],$_POST['apPaterno'],$_POST['apMaterno'],$_POST['rfc']);
      if ($insertar) {
        $respuesta['error'] = false;
        $respuesta['msg'] = "Cliente registrado correctamente.";
      }else {
        $respuesta['error'] = true;
        $respuesta['msg'] = "Error al registrar cliente.";
      }
    }else {
      $respuesta['error'] = true;
      $respuesta['msg'] = "Faltan parametros.";
    }
    echo json_encode($respuesta);

}





 ?>
