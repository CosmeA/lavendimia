<?php

require 'conexion.php';

  $accion = $_POST["accion"];

  switch ($accion) {
    case 1:
      obtenerVentas();
      break;
    case 2:
      registrarVentas();
      break;
    default:
      break;
  }

function obtenerVentas()
{
  $respuesta = array();
  $ventas = getVentas();
  $respuesta['clientes'] = getClientes();
  $respuesta['articulos'] = getArticulosExistentes();

  if (count($ventas) > 0) {
    $respuesta['error'] = false;
    $respuesta['datos'] = $ventas;

  }else {
    $respuesta['error'] = true;
    $respuesta['datos'] = array();
  }
  echo json_encode($respuesta);
}




function registrarVentas()
{
    $respuesta = array();

    if (isset($_POST['cliente']) && isset($_POST['articulo']) && isset($_POST['importe']) ) {
      $insertar = insertarVenta($_POST['cliente'],$_POST['articulo'],$_POST['importe']);
      if ($insertar > 0) {
        descontarArticulosVenta($_POST['articulo']);

        $respuesta['error'] = false;
        $respuesta['msg'] = "Venta registrada correctamente.";
      }else {
        $respuesta['error'] = true;
        $respuesta['msg'] = "Error al registrar venta.";
      }
    }else {
      $respuesta['error'] = true;
      $respuesta['msg'] = "Faltan parametros.";
    }
    echo json_encode($respuesta);

}







 ?>
