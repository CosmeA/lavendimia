<?php

 function conexion()
{
  return mysqli_connect("127.0.0.1", "root", "kmitnick", "lavendimia");
}

 function cerrarConexion($enlace)
{
  mysqli_close($enlace);
}


function insertarCliente($nombre,$appaterno,$apmaterno,$rfc)
{
  $conexion = conexion();
  $query = "insert into clientes(nombre, apPaterno, apMaterno, rfc) Values('$nombre','$appaterno','$apmaterno','$rfc')";
  $resultado = mysqli_query($conexion,$query);
  cerrarConexion($conexion);
  return $resultado;
}

function insertarArticulo($desc,$modelo,$precio,$exist,$id)
{
  $conexion = conexion();
  if ($id > 0) {
    $query = "update articulos set descripcion = '$desc', modelo = '$modelo', precio = $precio, existencia = $exist where id = $id";
  }else {
    $query = "insert into articulos(descripcion, modelo, precio, existencia) Values('$desc','$modelo',$precio,$exist)";
  }
  $resultado = mysqli_query($conexion,$query);
  cerrarConexion($conexion);
  return $resultado;
}

function getClientes()
{
  $respuesta = array();
  $conexion = conexion();
  $query = "select * from clientes";
  $resultado = mysqli_query($conexion,$query);

  if ($resultado->num_rows > 0) {
      while($row = $resultado->fetch_assoc()) {
        array_push($respuesta,$row);
      }
  }

  return $respuesta;

}

function getArticulosExistentes()
{
  $respuesta = array();
  $conexion = conexion();
  $query = "select * from articulos where existencia > 0";
  $resultado = mysqli_query($conexion,$query);

  if ($resultado->num_rows > 0) {
      while($row = $resultado->fetch_assoc()) {
        array_push($respuesta,$row);
      }
  }

  return $respuesta;

}


function getArticulos()
{
  $respuesta = array();
  $conexion = conexion();
  $query = "select * from articulos";
  $resultado = mysqli_query($conexion,$query);

  if ($resultado->num_rows > 0) {
      while($row = $resultado->fetch_assoc()) {
        array_push($respuesta,$row);
      }
  }

  return $respuesta;

}

function getVentas()
{
  $respuesta = array();
  $conexion = conexion();
  $query = "select v.*,c.nombre from ventas v
            inner join clientes c on v.cliente = c.id";
  $resultado = mysqli_query($conexion,$query);

  if ($resultado->num_rows > 0) {
      while($row = $resultado->fetch_assoc()) {
        array_push($respuesta,$row);
      }
  }
  cerrarConexion($conexion);

  return $respuesta;

}


 function insertarVenta($cliente,$articulos,$importe)
{
  $conexion = conexion();
  $query = "insert into ventas(cliente, total, fecha, estado) Values($cliente,$importe,now(),1)";
   mysqli_query($conexion,$query);

  $resultado = $conexion->insert_id ;

  cerrarConexion($conexion);
  return $resultado;
}

function getConfiguracion()
{
  $respuesta = array();
  $conexion = conexion();
  $query = "select * from configuracion limit 1;";
  $resultado = mysqli_query($conexion,$query);

  if ($resultado->num_rows > 0) {
      while($row = $resultado->fetch_assoc()) {
        array_push($respuesta,$row);
      }
  }
  cerrarConexion($conexion);

  return $respuesta;
}

function editConfiguracion($tasa,$enganche,$plazo,$id = 0)
{
 $conexion = conexion();
 if ($id > 0) {
   $query = "update configuracion set tasa = $tasa,enganche = $enganche,plazo = $plazo where id = $id;";
 }else {
   $query = "insert into configuracion( tasa, enganche, plazo) Values($tasa,$enganche,$plazo);";
 }
 $resultado = mysqli_query($conexion,$query);
 cerrarConexion($conexion);
 return $resultado;
}

function descontarArticulosVenta($articulos)
{
  $conexion = conexion();

  foreach ($articulos as $articulo) {
    $query = "update articulos set existencia = (existencia - $articulo[cantidad]) where id = $articulo[producto]";
    $resultado = mysqli_query($conexion,$query);
    cerrarConexion($conexion);
  }
  return $resultado;
}


 ?>

