<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



require 'conexion.php';


  $accion = $_POST["accion"];

  switch ($accion) {
    case 1:
      obtenerConfiguracion();
      break;
    case 2:
      editarConfiguracion();
      break;
    default:
      break;
  }


function obtenerConfiguracion()
{
  $respuesta = array();
  $config = getConfiguracion();
  if (count($config) > 0) {
    $respuesta['error'] = false;
    $respuesta['datos'] = $config[0];
  }else {
    $respuesta['error'] = true;
    $respuesta['datos'] = array();
  }
  echo json_encode($respuesta);
}



function editarConfiguracion()
{
    $respuesta = array();

    if (isset($_POST['tasa']) && isset($_POST['enganche']) && isset($_POST['plazo']) ) {
      $id = isset($_POST['id']) ? $_POST['id'] : 0;
      $insertar = editConfiguracion($_POST['tasa'],$_POST['enganche'],$_POST['plazo'],$id);
      if ($insertar) {
        $respuesta['error'] = false;
        $respuesta['msg'] = "Configuración modificada correctamente.";
      }else {
        $respuesta['error'] = true;
        $respuesta['msg'] = "Error al modificar configuracion.";
      }
    }else {
      $respuesta['error'] = true;
      $respuesta['msg'] = "Faltan parametros.";
    }
    echo json_encode($respuesta);

}







 ?>
