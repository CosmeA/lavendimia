<?php

require 'conexion.php';

  $accion = $_POST["accion"];

  switch ($accion) {
    case 1:
      obtenerArticulos();
      break;
    case 2:
     registrarArticulos();
    default:
      break;
  }

function obtenerArticulos()
{
  $respuesta = array();
  $articulos = getArticulos();
  if (count($articulos) > 0) {
    $respuesta['error'] = false;
    $respuesta['datos'] = $articulos;
  }else {
    $respuesta['error'] = true;
    $respuesta['datos'] = array();
  }
  echo json_encode($respuesta);
}




function registrarArticulos()
{
    $respuesta = array();

    if (isset($_POST['descripcion']) && !empty($_POST['descripcion']) &&
        isset($_POST['modelo']) && !empty($_POST['modelo']) &&
        isset($_POST['precio']) && !empty($_POST['precio']) &&
        isset($_POST['existencia']) && !empty($_POST['existencia'])
       ) {
         $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $insertar = insertarArticulo($_POST['descripcion'],$_POST['modelo'],$_POST['precio'],$_POST['existencia'],$id);
      if ($insertar) {
        $respuesta['error'] = false;
        $respuesta['msg'] = "Articulo registrado correctamente.";
      }else {
        $respuesta['error'] = true;
        $respuesta['msg'] = "Error al registrar Articulo.";
      }
    }else {
      $respuesta['error'] = true;
      $respuesta['msg'] = "Faltan parametros.";
    }
    echo json_encode($respuesta);

}





 ?>
